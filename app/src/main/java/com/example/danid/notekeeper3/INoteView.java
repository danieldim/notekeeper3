package com.example.danid.notekeeper3;

/**
 * Created by danid on 12/24/2017.
 */

public interface INoteView {
    void getData();
    String getMeString();
    int getMeInt();
}
